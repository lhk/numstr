/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   d.mayer@itv.rwth-aachen.de                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <iostream>
#include <fstream>

#include <string.h>
#include <stdio.h>
#include <math.h>

#include "input.h"
#include "data.h"

//------------------------------------------------------
bool input( const char* cfgFilePath, const char* meshFilePath, sData* data ) {

   int section, lineNo;
   sCell*  curCell=0;
   sPoint* curPoint=0;
   char    line[256]=" ";
   char    token[32]=" ";
   int     pointId;
   double  bValue;
   int     bType;
   double  x;
   double  y;
   int     cellId;
   double  initPhi;

   //////////////////////
   // READ JOB FILE  //
   //////////////////////

   // open input file
   std::ifstream cfgFile( cfgFilePath );
   if( !cfgFile ) {
      return false;
   }
   // read input file line by line
   lineNo=0;
   while( !cfgFile.eof() ) {
      lineNo++;
      cfgFile.getline( line,255 );
      if( sscanf( line,"%15s",token )<1 ) {
         continue;
      };

      if( !strcmp( token,"#" ) ) {
         // skip comment lines
         // numerical settings
      }
      else if( !strcmp( token,"maxTime" ) ) {
         if( sscanf( line,"%15s %lf",token,&data->maxTime )		!= 2 ) {
            return error( cfgFilePath,lineNo,line );
         };
      }
      else if( !strcmp( token,"maxIter" ) ) {
         if( sscanf( line,"%15s %d",token,&data->maxIter )		!= 2 ) {
            return error( cfgFilePath,lineNo,line );
         };
      }
      else if( !strcmp( token,"residuum" ) ) {
         if( sscanf( line,"%15s %lf",token,&data->residuum )	!= 2 ) {
            return error( cfgFilePath,lineNo,line );
         };
         // physical settings
      }
      else if( !strcmp( token,"alpha" ) ) {
         if( sscanf( line,"%15s %lf",token,&data->alpha )		!= 2 ) {
            return error( cfgFilePath,lineNo,line );
         };
      }
      else if( !strcmp( token,"dt" ) ) {
         if( sscanf( line,"%15s %lf",token,&data->dt )		!= 2 ) {
            return error( cfgFilePath,lineNo,line );
         };
      }
      else if( !strcmp( token,"rho" ) ) {
         if( sscanf( line,"%15s %lf",token,&data->rho )			!= 2 ) {
            return error( cfgFilePath,lineNo,line );
         };
      }
      else if( !strcmp( token,"velocity" ) ) {
         if( sscanf( line,"%15s %lf %lf",token,&data->u,&data->v ) != 3 ) {
            return error( cfgFilePath,lineNo,line );
         };
      }
      else {
         std::cout << "unknown token: " << token << std::endl;
         return error( cfgFilePath,lineNo,line );
      }
   }
   cfgFile.close();

   /////////////////////
   // READ FACE FILE  //
   /////////////////////

   // open face file
   std::ifstream meshFile( meshFilePath );
   if( !meshFile ) {
      return false;
   }

   // read face data
   section=0;
   lineNo=0;
   int nX, nY;
   while( !meshFile.eof() ) {
      lineNo++;
      meshFile.getline( line,255 );
      if( sscanf( line,"%31s",token )<1 ) {
         continue;
      };

      if( !strcmp( token,"#" ) ) {
         // skip comment lines
      }
      else if( !strcmp( token,"pointDimensions" ) ) {
         if( sscanf( line,"%31s",token )	!= 1 ) {
            return error( meshFilePath,lineNo,line );
         };
         section=1;
      }
      else if( !strcmp( token,"points" ) ) {
         if( sscanf( line,"%31s",token )					!= 1 ) {
            return error( meshFilePath,lineNo,line );
         };
         section=2;
      }
      else if( !strcmp( token,"boundaryConditions" ) ) {
         if( sscanf( line,"%31s",token )					!= 1 ) {
            return error( meshFilePath,lineNo,line );
         };
         section=3;
      }
        else if( !strcmp( token,"initialConditionsPhi" ) ) {
         if( sscanf( line,"%31s",token )					!= 1 ) {
            return error( meshFilePath,lineNo,line );
         };
         section=4;
      }  
      else if( !strcmp( token,"initialConditionsPressure" ) ) {
         if( sscanf( line,"%31s",token )					!= 1 ) {
            return error( meshFilePath,lineNo,line );
         };
         section=5;
      } 
      else if( section==1 ) {	// reading dimensions
         if( sscanf( line,"%d %d",&nX,&nY )	!= 2 ) {
            return error( meshFilePath,lineNo,line );
         };
         data->nPointsX = nX;
         data->nPointsY = nY;
         data->nPoints  = nX*nY;
         data->nCells   = ( nX-1 )*( nY-1 );
         data->nFaces   = 2*nX*nY - nX - nY;
         data->points   = new sPoint[data->nPoints];
         data->cells    = new sCell[data->nCells];
         data->faces    = new sFace[data->nFaces];
         memset(data->points, 0, data->nPoints * sizeof(sPoint));
         memset(data->cells, 0, data->nCells * sizeof(sCell));
         memset(data->faces, 0, data->nFaces * sizeof(sFace));
      }
      else if( section==2 ) {	// reading point data section
         if( sscanf( line,"%d %lf %lf",&pointId,&x,&y )	!= 3 ) {
            return error( meshFilePath,lineNo,line );
         }
         curPoint     = &data->points[pointId];
         curPoint->id = pointId;
         curPoint->x  = x;
         curPoint->y  = y;
      }
      else if( section==3 ) {	// reading boundary condition section
         if( sscanf( line,"%d %d %lf",&cellId,&bType,&bValue )	!= 3 ) {
            return error( meshFilePath,lineNo,line );
         }
         curCell         = &data->cells[cellId];
         curCell->id     = cellId;
         curCell->bType  = bType;
         curCell->bValue = bValue;
      }
      else if( section==4 ) {	// reading initial condition section
         if( sscanf( line,"%d %lf",&cellId,&initPhi )	!= 2 ) {
            return error( meshFilePath,lineNo,line );
         }
         curCell         = &data->cells[cellId];
         curCell->phi    = initPhi;
      }
      else if( section==5 ) {	
         double initP;
         if( sscanf( line,"%d %lf",&cellId,&initP )	!= 2 ) {
            return error( meshFilePath,lineNo,line );
         }
         curCell         = &data->cells[cellId];
         curCell->p    = 0;
      }
   }
   meshFile.close();
   return true;
}

//------------------------------------------------------
bool error( const char* filePath, int lineNo, const char* line ) {
   std::cout << "ERROR reading " << filePath << ", line " << lineNo << ":" << std::endl;
   std::cout << "\t" << line << std::endl << std::endl;
   return false;
}
