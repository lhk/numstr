/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   d.mayer@itv.rwth-aachen.de                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <iostream>

#include <stdio.h>
#include <math.h>

#include "solve.h"
#include "data.h"

using namespace std;

#define SQ(x) ((x)*(x))

bool solve_CalcFlux(sData* data);
void calcFlux(sData* data);
bool solvePe(sData* data);
double A(double Pe);
double get_phi(sCell* cell);


void writeGridSize(sData* data){    
    ofstream gridsize("gridsize.dat");
    gridsize << data->nPointsX-1 << " " << data->nPointsY-1 << " " << data->maxIter;
}

void coefficients(sData* data,double dx,double dy,double vn,double ue,double vs,double uw, double& a_n, double& a_e, double& a_s, double& a_w, double& avp, double& atp){
    
    double D_e=data->alpha/dx;
    double D_w=data->alpha/dx;
    double D_n=data->alpha/dy;
    double D_s=data->alpha/dy;

    double Pe_n=data->rho * vn * dy / data->alpha;
    double Pe_e=data->rho * ue * dx / data->alpha;
    double Pe_s=data->rho * vs * dy / data->alpha;
    double Pe_w=data->rho * uw * dx / data->alpha;

    a_n=D_n * dx * A(fabs(Pe_n)) + MAX(-vn * data->rho * dx,0);
    a_e=D_e * dy * A(fabs(Pe_e)) + MAX(-ue * data->rho  * dy,0);
    a_s=D_s * dx * A(fabs(Pe_s)) + MAX( vs * data->rho  * dx,0);
    a_w=D_w * dy * A(fabs(Pe_w)) + MAX( uw * data->rho  * dy,0);

    avp= data->rho * dx * dy / data->dt;
    atp= a_n + a_e + a_s + a_w + avp;
}

void writeU(ofstream& file, sData* data){
    for(int cId=0; cId<data->nCells; cId++){
        sCell* cell = &(data->cells[cId]);
        double u = .5*(cell->faces[XP]->velocity+cell->faces[XM]->velocity);
        file << u << " ";
    }
}
void writeV(ofstream& file, sData* data){
    for(int cId=0; cId<data->nCells; cId++){
        sCell* cell = &(data->cells[cId]);
        double v = .5*(cell->faces[YP]->velocity+cell->faces[YM]->velocity);
        file << v << " ";
    }
}


void solveVelocityField(sData* data){
    ofstream u_file("u.dat");
    ofstream v_file("v.dat");
    writeGridSize(data);
    
    for( int i=0; i < data->maxIter; i++ ) // time integration dt
    {
        writeU(u_file, data);
        writeV(v_file, data);
        
        
        for(int cId=0; cId<data->nCells; cId++){ // reset pressure
            sCell* cell = &(data->cells[cId]);
            if(cell->bType != INNERCELL) continue;
            //cell->p = 1;
            cell->p_correction = 0;
        }

        double eps=1e-3;
    
        double maxPressureResidual = 999;
        while(maxPressureResidual > eps) {
            maxPressureResidual = 0;
            
            for( int fId=0; fId < data->nFaces; fId++ ) { // momentum equation
                sFace* curFace=&data->faces[fId];
                if(curFace->bType != INNERCELL)
                    continue;
                
                sFace* face_n = curFace->neighFaces[YP];
                sFace* face_s = curFace->neighFaces[YM];
                sFace* face_w = curFace->neighFaces[XM];
                sFace* face_e = curFace->neighFaces[XP];        
                if(face_n == NULL || face_s == NULL || face_w == NULL || face_e == NULL) continue;
                
                double deltaP = curFace->neighCells[M]->p - curFace->neighCells[P]->p;
                
                if(curFace->dx > curFace->dy){ // horizontal edge
                    double v_p = curFace->velocity;
                    double v_w = face_w->velocity;
                    double v_e = face_e->velocity;
                    double v_s = face_s->velocity;
                    double v_n = face_n->velocity;
                    
                    double u_e = .5 * (curFace->diagNeighFaces[XPYP]->velocity + curFace->diagNeighFaces[XPYM]->velocity);
                    double u_w = .5 * (curFace->diagNeighFaces[XMYP]->velocity + curFace->diagNeighFaces[XMYM]->velocity);
                    
                    double a_n, a_e, a_s, a_w, avp, atp;
                    
                    coefficients(data, curFace->neighCells[M]->dx, curFace->neighCells[M]->dy, .5*(v_n+v_p), u_e, .5*(v_s+v_p), u_w, 
                         a_n,  a_e,  a_s,  a_w,  avp,  atp);
                         
                    double newVelocity = (1./atp) *
                        (a_n * v_n
                        +a_e * v_e
                        +a_s * v_s
                        +a_w * v_w
                        +avp * v_p
                        + deltaP * curFace->dx);
                        
                    //maxResidual = MAX(maxResidual, v_p - newVelocity);
                    curFace->velocity_next = newVelocity;
                    curFace->atp = atp;
                } 
                else { // vertical edge
                    double u_p = curFace->velocity;
                    double u_w = face_w->velocity;
                    double u_e = face_e->velocity;
                    double u_s = face_s->velocity;
                    double u_n = face_n->velocity;
                    
                    double v_n = .5 * (curFace->diagNeighFaces[XMYP]->velocity + curFace->diagNeighFaces[XPYP]->velocity);
                    double v_s = .5 * (curFace->diagNeighFaces[XMYM]->velocity + curFace->diagNeighFaces[XPYM]->velocity);
                    
                    double a_n, a_e, a_s, a_w, avp, atp;
                    
                    coefficients(data, curFace->neighCells[M]->dx, curFace->neighCells[M]->dy, v_n, .5*(u_p+u_e), v_s, .5*(u_p+u_w), 
                         a_n,  a_e,  a_s,  a_w,  avp,  atp);
                         
                    double newVelocity = (1./atp) *
                        (a_n * u_n
                        +a_e * u_e
                        +a_s * u_s
                        +a_w * u_w
                        +avp * u_p
                        + deltaP * curFace->dy);
                        
                    //maxResidual = MAX(maxResidual, u_p - newVelocity);
                    curFace->velocity_next = newVelocity;
                    curFace->atp = atp;
                }
            } 
            
            
            for(int cId=0; cId<data->nCells; cId++){ // pressure correction
                sCell* cell = &(data->cells[cId]);
                if(cell->bType != INNERCELL) continue;
                double ae = data->rho * SQ(cell->dy) / cell->faces[XP]->atp;
                double aw = data->rho * SQ(cell->dy) / cell->faces[XM]->atp;
                double an = data->rho * SQ(cell->dx) / cell->faces[YP]->atp;
                double as = data->rho * SQ(cell->dx) / cell->faces[YM]->atp;
                
                double b = data->rho * ( cell->dy * (cell->faces[XM]->velocity_next - cell->faces[XP]->velocity_next)
                                       + cell->dx * (cell->faces[YM]->velocity_next - cell->faces[YP]->velocity_next)  );
                                       
                double atp_new = ae + aw + an + as;
                
                cell->p_correction = (1./atp_new) * (
                    ae * cell->neighCells[XP]->p_correction
                  + aw * cell->neighCells[XM]->p_correction
                  + an * cell->neighCells[YP]->p_correction
                  + as * cell->neighCells[YM]->p_correction
                  + b
                );
                
            }
            
            for(int cId=0; cId<data->nCells; cId++){ // pressure update
                sCell* cell = &(data->cells[cId]);
                if(cell->bType == INNERCELL){
                    maxPressureResidual = MAX(maxPressureResidual, ABS(cell->p_correction));
                    cell->p += 0.7*cell->p_correction;
                    
                } /*
                else if (cell->bType == NEUMANN) {
                    sCell* neighborCell = NULL;
                    if(cell->neighCells[XP] != NULL && cell->neighCells[XP]->bType == INNERCELL) neighborCell = cell->neighCells[XP];
                    else if(cell->neighCells[XM] != NULL && cell->neighCells[XM]->bType == INNERCELL) neighborCell = cell->neighCells[XM];
                    else if(cell->neighCells[YP] != NULL && cell->neighCells[YP]->bType == INNERCELL) neighborCell = cell->neighCells[YP];
                    else if(cell->neighCells[YM] != NULL && cell->neighCells[YM]->bType == INNERCELL) neighborCell = cell->neighCells[YM];
                    
                    if(neighborCell != NULL) {
                        cell->p = neighborCell->p;
                    }
                }*/
            }
            
            /*for(int cId=0; cId<data->nCells; cId++){ // pressure update
                sCell* cell = &(data->cells[cId]);
                
                if(cell->x > 1-cell->dx) {
                    cell->bType = NEUMANN;
                    //cell->p = cell->neighCells[XM]->p;
                    //cout << cell->bType << " | " << cell->neighCells[XM]->bType << endl;
                }
                else if(cell->x < cell->dx-1)
                    cell->bType = NEUMANN;
                    //cell->p = cell->neighCells[XP]->p;
                else if(cell->y < cell->dy-1)
                    cell->bType = NEUMANN;
                    //cell->p = cell->neighCells[YP]->p;                
            }*/
            
            for( int fId=0; fId < data->nFaces; fId++ ) { 
                sFace* curFace=&data->faces[fId];
                sCell* cell_p = curFace->neighCells[P];
                sCell* cell_m = curFace->neighCells[M];
                if(cell_p != NULL && cell_m != NULL) {
                    if(cell_p->bType == NEUMANN && cell_m->bType == INNERCELL)
                        cell_p->p = cell_m->p;
                    else if(cell_p->bType == INNERCELL && cell_m->bType == NEUMANN)
                        cell_m->p = cell_p->p;
                }
            }
            
            
        }
        
        for( int fId=0; fId < data->nFaces; fId++ ) { // apply new velocity
            sFace* curFace=&data->faces[fId];
            /*
            if(curFace->neighCells[M]!=NULL && curFace->neighCells[P]!=NULL)
                if( curFace->neighCells[M]->bType && curFace->neighCells[P]->bType != curFace->bType)
                    cout << curFace->neighCells[M]->bType << " " << curFace->neighCells[P]->bType << " - " << curFace->bType << endl;
            */
            
            if(curFace->bType != INNERCELL) continue;
            curFace->velocity = curFace->velocity_next;
            curFace->velocity_next = 0;
        }
        cout << maxPressureResidual << " " << i << endl;
    }
}



//------------------------------------------------------
bool solve(sData* data){
    //return solve_CalcFlux(data);
    //return solvePe(data);
    solveVelocityField(data);
    return true;
}


void writePhi(ofstream& file, sData* data){
    for(int cId=0; cId<data->nCells; cId++){
        sCell* cell = &(data->cells[cId]);
        double phi = cell->phi;
        if(cell->bType != INNERCELL)
            phi = cell->bValue;
            
        file << phi << " ";        
    }    
}





// -------------------------------
// General solver... A(|Pe|)
bool solvePe(sData* data)
{
   std::cout << "\nCalculation:\n------------\n";
    ofstream phi_size_file("phi_size.dat");
    phi_size_file << data->nPointsX-1 << " " << data->nPointsY-1 << " " << data->maxIter;
    ofstream phi_file("phi.dat");
   sCell* curCell=0;
   //sFace* curFace=0;
   
   for(int j=0; j<data->maxIter; j++){
       
       writePhi(phi_file, data);
       
       for(int cId=0; cId<data->nCells; cId++){
           curCell=&data->cells[cId];
           
            //there is no flux for cells on the boundary.
           if(curCell->bType!=INNERCELL)
               continue;
           
           sCell* c_n, *c_e, *c_s, *c_w;
           c_n=curCell->neighCells[YP];
           c_e=curCell->neighCells[XP];
           c_s=curCell->neighCells[YM];
           c_w=curCell->neighCells[XM];
           
           double c_n_phi=get_phi(c_n);
           double c_e_phi=get_phi(c_e);
           double c_s_phi=get_phi(c_s);
           double c_w_phi=get_phi(c_w);
           
           double a_n,a_e,a_s,a_w,avp,atp;
                      
           coefficients(data,curCell->dx,curCell->dy,data->v,data->u,data->v,data->u, a_n, a_e, a_s, a_w, avp, atp);
           
           double b = avp * curCell->phi;
           curCell->phi= (a_e*c_e_phi + a_w*c_w_phi + a_n*c_n_phi + a_s*c_s_phi + b) / atp;
       }
   }
   return true;
}



double A(double Pe){
    // choose the method
    return 1;
}

double get_phi(sCell* cell){
    if(cell->bType==INNERCELL)
      return cell->phi;
    else if(cell->bType==DIRICHLET)
      return cell->bValue;
    else
    {
      std::cout<<"neumann boundary values are not implemented yet"<<std::endl;
      exit(-1);
    }
}


/*bool solve_CalcFlux(sData* data)
{
    std::cout << "\nCalculation:\n------------\n";
    double dt = data->dt;
    // FIXME
    for(int iter=0; iter<data->maxIter; iter++){
        std::cout << data->cells[4].phi << std::endl;
        calcFlux(data);
        for( int cellId=0; cellId < data->nCells; cellId++ ) {
            sCell* curCell = &data->cells[cellId];
            if(curCell->bType == INNERCELL) {
                double flow_sum = 0;
                
                flow_sum += curCell->faces[XM]->numFlux[0] * curCell->faces[XM]->dy;
                flow_sum -= curCell->faces[XP]->numFlux[0] * curCell->faces[XP]->dy;            
                
                flow_sum += curCell->faces[YM]->numFlux[1] * curCell->faces[YM]->dx;
                flow_sum -= curCell->faces[YP]->numFlux[1] * curCell->faces[YP]->dx;
                
                curCell->phi += dt * flow_sum / data->rho / curCell->volume;
            }
        }
    }
    
    sFace* f = data->cells[0].faces[0];
    double l = MAX(f->dx,f->dy);
    double u = MAX(data->u,data->v);
    double Pe = data->rho * u * l / data->alpha;
    std::cout << "Pe: " << Pe << std::endl;
    

    std::cout << "\n";
    return true;
}

//------------------------------------------------------
void calcFlux(sData* data)
{
   static sFace* curFace=0;

   // compute numerical flux of each face
   for(int fId=0; fId<data->nFaces; fId++) {
      curFace=&data->faces[fId];
      // FIXME      
      curFace->numFlux[0] = 0;
      curFace->numFlux[1] = 0;
      
      sCell* c0 = curFace->neighCells[0];
      sCell* c1 = curFace->neighCells[1];
      
      if(c0 != NULL && c1 != NULL) {
          
          double c0_phi = c0->phi;
          double c1_phi = c1->phi;
          
          if(c0->bType == DIRICHLET) c0_phi = c0->bValue;
          if(c1->bType == DIRICHLET) c1_phi = c1->bValue;
          
          double dx_cell = c1->x - c0->x;
          double dy_cell = c1->y - c0->y;
          
          
          
          // linear
          // double phi_face = 0.5 * (c1_phi + c0_phi);
          
          // upwind
          double phi_face;
          if(curFace->dx > curFace->dy){
              if(data->v>0) phi_face = c0_phi;
              else phi_face = c1_phi;
          }
          else {
              if(data->u>0) phi_face = c0_phi;
              else phi_face = c1_phi;
          }
          
          
          double dphi_dx = 0;
          double dphi_dy = 0;
          
          if(dx_cell != 0) dphi_dx = (c1_phi - c0_phi) / dx_cell;
          else if(dy_cell != 0) dphi_dy = (c1_phi - c0_phi) / dy_cell;
          else std::cout << "Error: non-cartesian grid." << std::endl;
          
          double flux_x = data->rho * data->u * phi_face - data->alpha * dphi_dx;
          double flux_y = data->rho * data->v * phi_face - data->alpha * dphi_dy;
          
          curFace->numFlux[0] = flux_x;
          curFace->numFlux[1] = flux_y;          
      }
   }
}*/