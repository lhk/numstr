/***************************************************************************
 *   Copyright (C) 2006-2011 by  Institute of Combustion Technology        *
 *   d.mayer@itv.rwth-aachen.de                                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <iostream>

#include <stdio.h>
#include <math.h>

#include "setup.h"
#include "data.h"
#include "output.h"

using namespace std;

const double omega = 2;


double U(sData *data, double x, double y, int btype) {

    // laminar flow into the area, obstacle within the area
    cout << " " << x << " ";
    //return ABS(x) > 0.9 || ABS(y) > 0.9 ? 100 : 0;
    //return (x < -1. +2./(data->nPointsX-1)) ? 100:0;

    // driven cavity
    return (y > 1. - 2. / (data->nPointsY - 1)) ? 100 : 0;

    return 0;




    // quadrat profil
    if (false) {
        double u_o = 0;
        double u_u = 0;
        double dp = -10;
        double mu = data->rho * data->alpha;
        return btype == INNERCELL ? 2 : dp / mu * y * y + (u_o - u_u - dp / mu) * y + u_u;
    }


    // solid rotation
    if (false) {
        return btype == INNERCELL ? 2 : -omega * y;
    }


}

double V(sData *data, double x, double y, int btype) {

    // driven cavity
    return 0;


    // solid rotation
    if (false) {
        return btype == INNERCELL ? 2 : omega * x;
    }
}

double pressure(sData *data, double x, double y, int btype) {

    return 1000;
    return ABS(x - 1) / 2 * 1000;

    if (x < -0.9)
        return 100;
    if (ABS(y) > 0.9)
        return ABS(y - 1) / 2 * 100;
    else
        return 0;

    // for driven cavity
    return 0;


    // solid rotation
    return .5 * data->rho * omega * omega * (x * x + y * y);
}


//------------------------------------------------------
bool setup(sData *data) {

    sCell *curCell = NULL;
    sFace *curFace = NULL;
    int i, j;

    /////////////////////////////////
    // construct mesh connectivity //
    /////////////////////////////////
    for (int cellId = 0; cellId < data->nCells; cellId++) {
        // assign points to cells
        i = cellId % (data->nPointsX - 1);
        j = cellId / (data->nPointsX - 1);
        curCell = &data->cells[cellId];
        curCell->points[XMYM] = &data->points[j * data->nPointsX + i];
        curCell->points[XPYM] = &data->points[j * data->nPointsX + i + 1];
        curCell->points[XMYP] = &data->points[(j + 1) * data->nPointsX + i];
        curCell->points[XPYP] = &data->points[(j + 1) * data->nPointsX + i + 1];

        // assign faces to cells
        curCell->faces[YM] = &data->faces[cellId];
        curCell->faces[YP] = &data->faces[cellId + data->nPointsX - 1];
        curCell->faces[XM] = &data->faces[(data->nPointsX - 1) * data->nPointsY + cellId + j];
        curCell->faces[XP] = &data->faces[(data->nPointsX - 1) * data->nPointsY + cellId + j + 1];

        // assign cells to faces
        curCell->faces[YM]->neighCells[P] = curCell;
        curCell->faces[YP]->neighCells[M] = curCell;
        curCell->faces[XM]->neighCells[P] = curCell;
        curCell->faces[XP]->neighCells[M] = curCell;

        // assign points to faces
        curCell->faces[YM]->points[M] = curCell->points[XMYM];
        curCell->faces[YM]->points[P] = curCell->points[XPYM];
        curCell->faces[YP]->points[M] = curCell->points[XMYP];
        curCell->faces[YP]->points[P] = curCell->points[XPYP];
        curCell->faces[XM]->points[M] = curCell->points[XMYM];
        curCell->faces[XM]->points[P] = curCell->points[XMYP];
        curCell->faces[XP]->points[M] = curCell->points[XPYM];
        curCell->faces[XP]->points[P] = curCell->points[XPYP];

        //assign neighboring cells to cells
        if (i != 0) {
            curCell->neighCells[XM] = &data->cells[cellId - 1];
        }
        if (i != data->nPointsX - 2) {
            curCell->neighCells[XP] = &data->cells[cellId + 1];
        }
        if (j != 0) {
            curCell->neighCells[YM] = &data->cells[cellId - (data->nPointsX - 1)];
        }
        if (j != data->nPointsY - 2) {
            curCell->neighCells[YP] = &data->cells[cellId + (data->nPointsX - 1)];
        }
    }


    // connect faces
    for (int cellId = 0; cellId < data->nCells; cellId++) {
        curCell = &data->cells[cellId];
        sCell *rightNeighborCell = curCell->neighCells[XP];
        if (rightNeighborCell != NULL) {
            curCell->faces[YM]->neighFaces[XP] = rightNeighborCell->faces[YM];
            rightNeighborCell->faces[YM]->neighFaces[XM] = curCell->faces[YM];

            curCell->faces[YP]->neighFaces[XP] = rightNeighborCell->faces[YP];
            rightNeighborCell->faces[YP]->neighFaces[XM] = curCell->faces[YP];
        }

        sCell *topNeighborCell = curCell->neighCells[YP];
        if (topNeighborCell != NULL) {
            curCell->faces[XP]->neighFaces[YP] = topNeighborCell->faces[XP];
            topNeighborCell->faces[XP]->neighFaces[YM] = curCell->faces[XP];

            curCell->faces[XM]->neighFaces[YP] = topNeighborCell->faces[XM];
            topNeighborCell->faces[XM]->neighFaces[YM] = curCell->faces[XM];
        }

        curCell->faces[XP]->neighFaces[XM] = curCell->faces[XM];
        curCell->faces[XM]->neighFaces[XP] = curCell->faces[XP];

        curCell->faces[YP]->neighFaces[YM] = curCell->faces[YM];
        curCell->faces[YM]->neighFaces[YP] = curCell->faces[YP];

    }

    // connect faces diagonally
    for (int cellId = 0; cellId < data->nCells; cellId++) {
        curCell = &data->cells[cellId];

        curCell->faces[YM]->diagNeighFaces[XPYP] = curCell->faces[XP];
        curCell->faces[XP]->diagNeighFaces[XMYM] = curCell->faces[YM];

        curCell->faces[YM]->diagNeighFaces[XMYP] = curCell->faces[XM];
        curCell->faces[XM]->diagNeighFaces[XPYM] = curCell->faces[YM];

        curCell->faces[YP]->diagNeighFaces[XMYM] = curCell->faces[XM];
        curCell->faces[XM]->diagNeighFaces[XPYP] = curCell->faces[YP];

        curCell->faces[YP]->diagNeighFaces[XPYM] = curCell->faces[XP];
        curCell->faces[XP]->diagNeighFaces[XMYP] = curCell->faces[YP];
    }

    for (int fId = 0; fId < data->nFaces; fId++) {
        curFace = &data->faces[fId];
        double p0x = curFace->points[0]->x;
        double p0y = curFace->points[0]->y;
        double p1x = curFace->points[1]->x;
        double p1y = curFace->points[1]->y;
        curFace->x = .5 * (p0x + p1x);
        curFace->y = .5 * (p0y + p1y);
        curFace->dx = p1x - p0x;
        curFace->dy = p1y - p0y;
    }

    //////////////////////////////////////
    // compute cell centers and volumes //
    //////////////////////////////////////
    for (int cellId = 0; cellId < data->nCells; cellId++) {
        // FIXME
        // hint 1: assume cartesian mesh (for now)
        // hint 2: use cell points & neighboring face deltas
        curCell = &data->cells[cellId];
        curCell->x = 0;
        curCell->y = 0;
        for (int i = 0; i < 4; i++) {
            curCell->x += curCell->points[i]->x / 4.;
            curCell->y += curCell->points[i]->y / 4.;
        }
        curCell->volume = curCell->faces[XM]->dy * curCell->faces[YM]->dx;
        curCell->dx = ABS(curCell->faces[YM]->dx);
        curCell->dy = ABS(curCell->faces[XM]->dy);
    }


    // write correct ids to cells
    for (int cellId = 0; cellId < data->nCells; cellId++) {
        curCell = &data->cells[cellId];
        curCell->id = cellId;
    }

    for (int faceId = 0; faceId < data->nFaces; faceId++) {
        curFace = &data->faces[faceId];
        curFace->id = faceId;
    }
    // set cell btype
    for (int cellId = 0; cellId < data->nCells; cellId++) {
        curCell = &data->cells[cellId];


        // simple obstacle
        /*
        double eps = 1e-1;
        if (curCell->neighCells[YP] == NULL
            || curCell->neighCells[XM] == NULL
            || curCell->neighCells[XP] == NULL
            || curCell->neighCells[YM] == NULL)
            curCell->bType = DIRICHLET;
        else if (ABS(curCell->x + 0.5) < eps && ABS(curCell->y -0) < 0.2)
            curCell->bType=NEUMANN;
        else curCell->bType = INNERCELL;
        */

        //driven cavity
        if(curCell->neighCells[YP] == NULL)
            curCell->bType = DIRICHLET;
        else if (
           curCell->neighCells[XM] == NULL
        || curCell->neighCells[XP] == NULL
        || curCell->neighCells[YM] == NULL
        ) curCell->bType = NEUMANN;
        else
            curCell->bType = INNERCELL;

        cout << curCell->bType << " ";
    }
    cout << endl;


    for (int fId = 0; fId < data->nFaces; fId++) {
        curFace = &data->faces[fId];

        curFace->bType = INNERCELL;

        if (curFace->neighCells[P] == NULL
            || curFace->neighCells[M] == NULL
            || curFace->neighFaces[0] == NULL
            || curFace->neighFaces[1] == NULL
            || curFace->neighFaces[2] == NULL
            || curFace->neighFaces[3] == NULL)
            curFace->bType = DIRICHLET;

        //if(curFace->neighCells[P]==NULL || curFace->neighCells[M]==NULL)
        //    curFace->bType=DIRICHLET;

        if (curFace->neighCells[P] != NULL
            && curFace->neighCells[M] != NULL) {
            if (curFace->neighCells[P]->bType != INNERCELL && curFace->neighCells[M]->bType != INNERCELL) {
                curFace->bType = DIRICHLET;
            }
        }

        if (curFace->dx < curFace->dy) {
            curFace->velocity = U(data, curFace->x, curFace->y, curFace->bType);
        }
        else {
            curFace->velocity = V(data, curFace->x, curFace->y, curFace->bType);
        }

    }





    ////////////////////////////
    // set initial conditions //
    ////////////////////////////
    for (int cId = 0; cId < data->nCells; cId++) {
        curCell = &data->cells[cId];
        // FIXME
    }

    /////////////////////////////
    // set boundary conditions //
    /////////////////////////////
    for (int cId = 0; cId < data->nCells; cId++) {
        curCell = &data->cells[cId];
        // FIXME
        //curCell->bType = init_bType(data,curFace->x,curFace->y);
        curCell->p = pressure(data, curFace->x, curFace->y, curFace->bType);
    }

    return true;
}
