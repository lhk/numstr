.PHONY: clean All

All:
	@echo "----------Building project:[ Dragonfly - Debug ]----------"
	@"$(MAKE)" -f  "Dragonfly.mk"
clean:
	@echo "----------Cleaning project:[ Dragonfly - Debug ]----------"
	@"$(MAKE)" -f  "Dragonfly.mk" clean
