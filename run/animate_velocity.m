function animate_velocity( u,v )
close
figure('units','normalized','outerposition',[0 0 1 1])
quiver_handle = quiver(u(:,:,2)',v(:,:,2)');
for t=1:size(u,3)
    ut = u(:,:,t)';
    vt = v(:,:,t)';
    
%     mag = sqrt(ut.*ut+vt.*vt);
%     ut = ut ./ mag;
%     vt = vt ./ mag;
    
    set(quiver_handle, 'UData', ut);
    set(quiver_handle, 'VData', vt);
    pause(0.001);
    drawnow;
end
close

end

