function animate_phi( phi )
close
figure('units','normalized','outerposition',[0 0 1 1])
surf_handle = surf(phi(:,:,1000));
camproj perspective
cameratoolbar('SetMode','orbit') 
xlabel('x')
ylabel('y')
for t=1:size(phi,3)    
    set(surf_handle, 'ZData', phi(:,:,t));
    pause(0.401);
    drawnow;
end
close

end

