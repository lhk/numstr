##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Dragonfly
ConfigurationName      :=Debug
WorkspacePath          :=/home/lhk/programming/numströ/itv_ns/Dragonfly
ProjectPath            :=/home/lhk/programming/numströ/itv_ns/Dragonfly
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=lhk
Date                   :=20/06/16
CodeLitePath           :=/home/lhk/.codelite
LinkerName             :=/usr/bin/x86_64-linux-gnu-g++
SharedObjectLinkerName :=/usr/bin/x86_64-linux-gnu-g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="Dragonfly.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -O0
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch). $(LibraryPathSwitch)Debug 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/x86_64-linux-gnu-ar rcu
CXX      := /usr/bin/x86_64-linux-gnu-g++
CC       := /usr/bin/x86_64-linux-gnu-gcc
CXXFLAGS :=  -g -Wall $(Preprocessors)
CFLAGS   :=   $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/x86_64-linux-gnu-as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/src_solve.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_input.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_main.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_setup.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_output.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_solve.cpp$(ObjectSuffix): src/solve.cpp $(IntermediateDirectory)/src_solve.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/lhk/programming/numströ/itv_ns/Dragonfly/src/solve.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_solve.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_solve.cpp$(DependSuffix): src/solve.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_solve.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_solve.cpp$(DependSuffix) -MM "src/solve.cpp"

$(IntermediateDirectory)/src_solve.cpp$(PreprocessSuffix): src/solve.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_solve.cpp$(PreprocessSuffix) "src/solve.cpp"

$(IntermediateDirectory)/src_input.cpp$(ObjectSuffix): src/input.cpp $(IntermediateDirectory)/src_input.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/lhk/programming/numströ/itv_ns/Dragonfly/src/input.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_input.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_input.cpp$(DependSuffix): src/input.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_input.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_input.cpp$(DependSuffix) -MM "src/input.cpp"

$(IntermediateDirectory)/src_input.cpp$(PreprocessSuffix): src/input.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_input.cpp$(PreprocessSuffix) "src/input.cpp"

$(IntermediateDirectory)/src_main.cpp$(ObjectSuffix): ../Spider/src/main.cpp $(IntermediateDirectory)/src_main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/lhk/programming/numströ/itv_ns/Spider/src/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_main.cpp$(DependSuffix): ../Spider/src/main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_main.cpp$(DependSuffix) -MM "../Spider/src/main.cpp"

$(IntermediateDirectory)/src_main.cpp$(PreprocessSuffix): ../Spider/src/main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_main.cpp$(PreprocessSuffix) "../Spider/src/main.cpp"

$(IntermediateDirectory)/src_setup.cpp$(ObjectSuffix): ../Spider/src/setup.cpp $(IntermediateDirectory)/src_setup.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/lhk/programming/numströ/itv_ns/Spider/src/setup.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_setup.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_setup.cpp$(DependSuffix): ../Spider/src/setup.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_setup.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_setup.cpp$(DependSuffix) -MM "../Spider/src/setup.cpp"

$(IntermediateDirectory)/src_setup.cpp$(PreprocessSuffix): ../Spider/src/setup.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_setup.cpp$(PreprocessSuffix) "../Spider/src/setup.cpp"

$(IntermediateDirectory)/src_output.cpp$(ObjectSuffix): ../Spider/src/output.cpp $(IntermediateDirectory)/src_output.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/lhk/programming/numströ/itv_ns/Spider/src/output.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_output.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_output.cpp$(DependSuffix): ../Spider/src/output.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_output.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_output.cpp$(DependSuffix) -MM "../Spider/src/output.cpp"

$(IntermediateDirectory)/src_output.cpp$(PreprocessSuffix): ../Spider/src/output.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_output.cpp$(PreprocessSuffix) "../Spider/src/output.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


